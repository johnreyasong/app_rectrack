/*


"Hang on, we're almost there." Greg affirmed while carrying a kid to the town's healer as fast as he could.

The rain was heavy but he had to push through to save a life. And after what seemed like forever, they finally came to the healer's hut.

"Help! I need help!" Greg shouted, hoping anyone could hear him despite the thundering clouds. 

Fortunately, a lady named Yaena saw them through the window.

"Oh dear! Come inside, quick!"



*/