import state from "./state";
export default {
  setApplicantDetails(state, data) {
    state.applicantDetails = data;
  },
  setApplicantStage(state, data) {
    state.applicantStage = data;
  },
  setPositionsList(state, data) {
    state.positionList = data;
  },
  endorseToDepDone(state, data) {
    state.doneEndorseToDep = data;
  },
  keepDone(state, data) {
    state.doneKeep = data;
  },
  blacklistDone(state, data) {
    state.doneBlacklist = data;
  },
  rejectDone(state, data) {
    state.doneReject = data;
  },
  onlineDone(state, data) {
    state.doneOnline = data;
  },
  endorseForAssessDone(state, data) {
    state.doneEndorseForAssess = data;
  },
  retakeDone(state, data) {
    state.doneRetake = data;
  },
  createSchedDone(state, data) {
    state.doneCreateSched = data;
  },
  updateDone(state, data) {
    state.doneUpdate = data;
  },
  shortlistDone(state, data) {
    state.doneShortlist = data;
  },
  jobOfferDone(state, data) {
    state.doneJobOffer = data;
  },
  deployDone(state, data) {
    state.doneDeploy = data;
  },
  deployDateDone(state, data) {
    state.doneChangeDeployDate = data;
  },
  rate(state, data) {
    state.doneRate = data;
  },
  referralDone(state, data) {
    state.doneReferral = data;
  },
  returnApplicantDone(state, data) {
    state.doneReturnApplicant = data;
  },
  getApplicantsAll(state, data) {
    state.allReceive = data;
  }
};
