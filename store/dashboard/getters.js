export default {
  applicantDetails(state) {
    return state.applicantDetails;
  },
  applicantStage(state) {
    return state.applicantStage;
  },
  positionsList(state) {
    return state.positionList;
  },
  doneEndorseToDep(state) {
    return state.doneEndorseToDep;
  },
  doneKeep(state) {
    return state.doneKeep;
  },
  doneBlacklist(state) {
    return state.doneBlacklist;
  },
  doneReject(state) {
    return state.doneReject;
  },
  doneOnline(state) {
    return state.doneOnline;
  },
  doneEndorseForAssess(state) {
    return state.doneEndorseForAssess;
  },
  doneRetake(state) {
    return state.doneRetake;
  },
  doneCreateSched(state) {
    return state.doneCreateSched;
  },
  doneUpdate(state) {
    return state.doneUpdate;
  },
  doneShortlist(state) {
    return state.doneShortlist;
  },
  doneJobOffer(state) {
    return state.doneJobOffer;
  },
  doneDeploy(state) {
    return state.doneDeploy;
  },
  doneChangeDeployDate(state) {
    return state.doneChangeDeployDate;
  },
  doneRate(state) {
    return state.doneRate;
  },
  doneReferral(state) {
    return state.doneReferral;
  },
  doneReturnApplicant(state) {
    return state.doneReturnApplicant;
  }
};
