import axios from "axios";
import moment from "moment";
import io from "socket.io-client";

// ACTIONS/FUNCTIONS PER STAGE
export default {
  // ENDORSE TO OTHER DEPARTMENT
  endorseToOtherDepartment({ commit }, { token, applicant, pos, notifData }) {
    var user = localStorage.fname + " " + localStorage.lname;
    user = user.toLocaleUpperCase();
    var data = localStorage.id + "/" + applicant.id + "/" + pos;
    var id = applicant.id;

    axios({
      method: "PUT",
      url: `${this.$axios.defaults.baseURL}/users/endorse-to-dept/${data}`,
      headers: { Authorization: `Bearer ${token}` }
    }).then(
      result => {
        // UPDATE APPLICANT INFO
        axios({
          method: "POST",
          url: `${this.$axios.defaults.baseURL}/email/applicant/${id}/endorse-to-other-dept`,
          headers: { Authorization: `Bearer ${token}` },
          data: {
            endorserID: localStorage.id, // USER ID
            positionID: pos // POSITION ID
          }
        }).then(
          result => {
            const socket = io.connect(localStorage.serverLink, {
              secure: true
            });
            socket.emit("notificationAlerts", notifData);
            commit("endorseToDepDone", "Done");
          },
          error => {
            console.log(error);
            commit("endorseToDepDone", error);
          }
        );
      },
      error => {
        console.log(error);
        commit("endorseToDepDone", error);
      }
    );
  },

  // KEEP FOR REFERENCE
  keepForreference({ commit }, { token, applicant, remarks, notifData }) {
    var user = localStorage.fname + " " + localStorage.lname;
    user = user.toLocaleUpperCase();
    var id = applicant.id;
    axios({
      method: "PUT",
      url: `${this.$axios.defaults.baseURL}/users/status/kept-reference/${id}`,
      headers: { Authorization: `Bearer ${token}` },
      data: { remarks: remarks }
    }).then(
      result => {
        // UPDATE APPLICANT INFO
        axios({
          method: "POSt",
          url: `${this.$axios.defaults.baseURL}/email/applicant/${id}/keep-for-reference`,
          headers: { Authorization: `Bearer ${token}` }
        }).then(
          result => {
            const socket = io.connect(localStorage.serverLink, {
              secure: true
            });
            socket.emit("notificationAlerts", notifData);
            commit("keepDone", "Done");
          },
          error => {
            commit("keepDone", error);
          }
        );
      },
      error => {
        commit("keepDone", error);
      }
    );
  },

  // BLACKLIST
  blacklist({ commit }, { token, applicant, remarks, notifData }) {
    var user = localStorage.fname + " " + localStorage.lname;
    user = user.toLocaleUpperCase();
    var id = applicant.id;
    axios({
      method: "PUT",
      url: `${this.$axios.defaults.baseURL}/users/status/blacklist/${id}`,
      headers: {
        Authorization: `Bearer ${token}`
      },
      data: {
        remarks: remarks
      }
    }).then(
      result => {
        // UPDATE APPLCIANT INFO
        axios({
          method: "POST",
          url: `${this.$axios.defaults.baseURL}/email/applicant/${id}/blacklisted`,
          headers: { Authorization: `Bearer ${token}` }
        }).then(
          result => {
            const socket = io.connect(localStorage.serverLink, {
              secure: true
            });
            socket.emit("notificationAlerts", notifData);
            commit("blacklistDone", "Done");
          },
          error => {
            commit("blacklistDone", error);
          }
        );
      },
      error => {
        console.log(error.response);
        commit("blacklistDone", error.response.data.patched.msg);
      }
    );
  },

  // REJECT
  reject({ commit }, { token, applicant, remarks, userID, notifData }) {
    var user = localStorage.fname + " " + localStorage.lname;
    user = user.toLocaleUpperCase();
    var id = applicant.id;
    console.log(userID);
    axios({
      method: "PUT",
      url: `${this.$axios.defaults.baseURL}/users/status/reject/${id}`,
      headers: { Authorization: `Bearer ${token}` },
      data: { remarks: remarks, rejectedBy: userID }
    }).then(
      result => {
        axios({
          method: "POST",
          url: `${this.$axios.defaults.baseURL}/email/applicant/${id}/rejected`,
          headers: { Authorization: `Bearer ${token}` }
        }).then(
          result => {
            const socket = io.connect(localStorage.serverLink, {
              secure: true
            });
            socket.emit("notificationAlerts", notifData);
            commit("rejectDone", "Done");
          },
          error => {
            console.log(error);
            commit("rejectDone", error);
          }
        );
      },
      error => {
        console.log(error);
        commit("rejectDone", error);
      }
    );
  },

  // ONLINE EXAM & IQBE
  onlineExam({ commit }, { token, applicant, notifData }) {
    var applicantName = applicant.name.toLocaleUpperCase();
    var user = localStorage.fname + " " + localStorage.lname;
    user = user.toLocaleUpperCase();
    var notif =
      user + " permitted " + applicantName + " to take the Online Exams.";
    var id = applicant.id;

    // UPDATE APPLICANT INFO
    axios({
      method: "PUT",
      url: `${this.$axios.defaults.baseURL}/users/status/online-exam/${id}`,
      headers: { Authorization: `Bearer ${token}` }
    }).then(
      result => {
        // SEND EMAIL
        axios({
          method: "POST",
          url: `${this.$axios.defaults.baseURL}/email/applicant/${id}/online-exam`,
          headers: { Authorization: `Bearer ${token}` },
          data: {
            onlineExamLink: `${process.env.baseURL}/examLogin`,
            iqbeExamLink: `${process.env.baseURL}/iqbeLogin`,
          }
        }).then(
          result => {
            // ALERT
            const socket = io.connect(localStorage.serverLink, {
              secure: true
            });
            console.log("result", result);
            socket.emit("notificationAlerts", notifData);
            commit("onlineDone", "Done");
          },
          error => {
            console.log(error);
            commit("onlineDone", error);
          }
        );
      },
      error => {
        console.log(error);
        commit("rejectDone", error);
      }
    );
  },

  // ENDORSE FOR ASSESSMENT
  endorseForAssessment({ commit }, { token, applicant, notifData }) {
    var applicantName = applicant.name.toLocaleUpperCase();
    var user = localStorage.fname + " " + localStorage.lname;
    user = user.toLocaleUpperCase();
    var notif = user + " endorsed " + applicantName + " for assessment.";
    var id = applicant.id;
    let url = `${this.$axios.defaults.baseURL}/users/status/endorsement/${id}`;
    console.log("endorsed", notifData[0].stage);
    if (notifData[0].stage == "Received") {
      url = `${this.$axios.defaults.baseURL}/users/status/for-endorsement/${id}`;
    }
    axios({
      method: "PUT",
      url: url,
      headers: {
        Authorization: `Bearer ${token}`
      }
    }).then(
      result => {
        if (notifData[0].stage == "Received") {
          console.log("received");
          axios({
            method: "PUT",
            url: `${this.$axios.defaults.baseURL}/users/status/endorsement/${id}`,
            headers: {
              Authorization: `Bearer ${token}`
            }
          }).then(
            result => {
              console.log("forEndorsement", result);
              const socket = io.connect(localStorage.serverLink, {
                secure: true
              });
              socket.emit("notificationAlerts", notifData);
              commit("endorseForAssessDone", "Done");
            },
            error => {
              console.log(error);
              commit("endorseForAssessDone", error);
            }
          );
        }
        const socket = io.connect(localStorage.serverLink, { secure: true });
        socket.emit("notificationAlerts", notifData);
        commit("endorseForAssessDone", "Done");
      },
      error => {
        console.log(error);
        commit("endorseForAssessDone", error);
      }
    );
  },
  // ENDORSE FOR ASSESSMENT
  // forEndorsement({ commit }, { token, applicant, notifData }) {
  //   var applicantName = applicant.name.toLocaleUpperCase();
  //   var user = localStorage.fname + " " + localStorage.lname;
  //   user = user.toLocaleUpperCase();
  //   var notif = user + " endorsed " + applicantName + " for assessment.";
  //   var id = applicant.id;
  //   console.log("endorsed", token, applicant, notifData[0].stage);
  //   axios({
  //     method: "PUT",
  //     url: `${this.$axios.defaults.baseURL}/users/status/endorsement/${id}`,
  //     headers: {
  //       Authorization: `Bearer ${token}`
  //     }
  //   }).then(
  //     result => {
  //       console.log("forEndorsement", result);
  //       const socket = io.connect(localStorage.serverLink, { secure: true });
  //       socket.emit("notificationAlerts", notifData);
  //       commit("endorseForAssessDone", "Done");
  //     },
  //     error => {
  //       console.log(error);
  //       commit("endorseForAssessDone", error);
  //     }
  //   );
  // },

  // RETAKE ONLINE EXAM & IQBE
  retake({ commit }, { token, applicant, notifData }) {
    var applicantName = applicant.name.toLocaleUpperCase();
    var user = localStorage.fname + " " + localStorage.lname;
    user = user.toLocaleUpperCase();
    var notif =
      user + " permitted " + applicantName + " to retake the Online Exams.";
    var id = applicant.id;
    axios({
      method: "PUT",
      url: `${this.$axios.defaults.baseURL}/users/online-exam-retake/${id}`,
      headers: { Authorization: `Bearer ${token}` }
    })
      .then(result => {
        // UPDATE APPLCIANT INFO
        axios({
          method: "POST",
          url: `${this.$axios.defaults.baseURL}/email/applicant/${id}/exam-retake`,
          headers: { Authorization: `Bearer ${token}` },
          data: {
            onlineExamLink: `${process.env.baseURL}/examLogin`,
            iqbeExamLink: `${process.env.baseURL}/iqbeLogin`
          }
        })
          .then(result => {
            const socket = io.connect(localStorage.serverLink, {
              secure: true
            });
            socket.emit("notificationAlerts", notifData);
            commit("retakeDone", "Done");
          })
          .catch(error => {
            console.log(error);
            commit("retakeDone", error);
          });
      })
      .catch(error => {
        console.log(error);
        commit("retakeDone", error);
      });
  },

  // CREATE SCHEDULE
  createSchedule(
    { commit },
    { token, applicant, dateTime, location, conP, conN, notifData }
  ) {
    var applicantName = applicant.name.toLocaleUpperCase();
    var user = localStorage.fname + " " + localStorage.lname;
    user = user.toLocaleUpperCase();
    var id = applicant.id;
    var notif =
      user + " created a schedule for " + applicantName + "'s assessment.";
    var dateF =
      dateTime.split("-")[0] +
      "/" +
      dateTime.split("-")[1] +
      "/" +
      dateTime.split("-")[2];
    axios({
      method: "PUT",
      url: `${this.$axios.defaults.baseURL}/users/endorsed/set-date/${id}`,
      headers: { Authorization: `Bearer ${token}` },
      data: {
        date: dateF,
        link: `${process.env.baseURL}/applicationFormLogin`,
        name: conP, // Contact Number
        contact: conN, // Contact Person
        location: location
      }
    }).then(
      result => {
        console.log("set date", result);
        axios({
          method: "POST",
          url: `${this.$axios.defaults.baseURL}/email/applicant/${id}/assessment-form`,
          headers: { Authorization: `Bearer ${token}` },
          data: {
            date: dateF,
            link: `${process.env.baseURL}/applicationFormLogin`,
            name: conP, // Contact Number
            contact: conN, // Contact Person
            location: location
          }
        }).then(
          result => {
            console.log("email", result);
            const socket = io.connect(localStorage.serverLink, {
              secure: true
            });
            socket.emit("notificationAlerts", notifData);
            commit("createSchedDone", "Done");
          },
          error => {
            console.log(error);
            commit("createSchedDone", error);
          }
        );
      },
      error => {
        console.log(error.response.data.error);
        commit("createSchedDone", error.response.data.error);
      }
    );
  },

  // UPDATE POSITION
  updatePosition({ commit }, { token, applicant, pos, notifData }) {
    console.log(pos);
    var applicantName = applicant.name.toLocaleUpperCase();
    var user = localStorage.fname + " " + localStorage.lname;
    user = user.toLocaleUpperCase();
    var id = applicant.id;
    var notif = user + " updated " + applicantName + "'s position.";
    var p = pos.id;
    axios({
      method: "PUT",
      url: `${this.$axios.defaults.baseURL}/users/position-update/${id}/${p}`,
      headers: { Authorization: `Bearer ${token}` }
    }).then(
      result => {
        const socket = io.connect(localStorage.serverLink, { secure: true });
        socket.emit("notificationAlerts", notifData);
        commit("updateDone", "Done");
      },
      error => {
        console.log(error);
        commit("updateDone", error);
      }
    );
  },

  // SHORTLIST
  shortlist({ commit }, { token, applicant, notifData }) {
    var applicantName = applicant.name.toLocaleUpperCase();
    var user = localStorage.fname + " " + localStorage.lname;
    user = user.toLocaleUpperCase();
    var notif = user + " shortlisted " + applicantName + ".";
    var id = applicant.id;
    axios({
      method: "PUT",
      url: `${this.$axios.defaults.baseURL}/users/status/shortlist/${id}`,
      headers: { Authorization: `Bearer ${token}` }
    }).then(
      result => {
        axios({
          method: "POST",
          url: `${this.$axios.defaults.baseURL}/email/applicant/${id}/shortlisted`,
          headers: { Authorization: `Bearer ${token}` }
        }).then(
          result => {
            const socket = io.connect(localStorage.serverLink, {
              secure: true
            });
            socket.emit("notificationAlerts", notifData);
            commit("shortlistDone", "Done");
            console.log("shorlist", result);
          },
          error => {
            console.log(error);
            commit("shortlistDone", error);
          }
        );
      },
      error => {
        console.log(error);
        commit("shortlistDone", error);
      }
    );
  },

  // JOB OFFER
  jobOffer(
    { commit },
    { token, applicant, date, time, location, conP, conN, notifData }
  ) {
    var applicantName = applicant.name.toLocaleUpperCase();
    var user = localStorage.fname + " " + localStorage.lname;
    user = user.toLocaleUpperCase();
    var id = applicant.id;
    var notif = user + " qualified " + applicantName + " for Job Offer.";
    var dateF =
      date.split("-")[0] + "/" + date.split("-")[1] + "/" + date.split("-")[2];
    var timeF = moment(time).format("hh:mm");
    commit("createSchedDone", "Done");
    axios({
      method: "PUT",
      url: `${this.$axios.defaults.baseURL}/users/status/job-offer/${id}`,
      headers: { Authorization: `Bearer ${token}` }
    }).then(
      result => {
        // UPDATE APPLCIANT INFO
        axios({
          method: "POST",
          url: `${this.$axios.defaults.baseURL}/email/applicant/${id}/job-offer`,
          headers: { Authorization: `Bearer ${token}` },
          data: {
            date: dateF,
            time: timeF,
            contactPerson: conP,
            contactNumber: conN,
            location: location
          }
        }).then(
          result => {
            const socket = io.connect(localStorage.serverLink, {
              secure: true
            });
            socket.emit("notificationAlerts", notifData);
            commit("jobOfferDone", "Done");
          },
          error => {
            console.log(error);
            commit("jobOfferDone", error);
          }
        );
      },
      error => {
        console.log(error);
        commit("jobOfferDone", error);
      }
    );
  },

  // DEPLOY
  deploy({ commit }, { token, applicant, data, notifData }) {
    var applicantName = applicant.name.toLocaleUpperCase();
    var user = localStorage.fname + " " + localStorage.lname;
    user = user.toLocaleUpperCase();
    var id = applicant.id;
    var notif = user + " deployed " + applicantName + ".";

    // Temporary deployment date
    var deployment =
      data.deployment.split("-")[0] +
      "/" +
      data.deployment.split("-")[1] +
      "/" +
      data.deployment.split("-")[2];
    // Final deployment date
    var deploymentF = deployment.split("T")[0];
    // Temporary deadline
    var deadline =
      data.deadline.split("-")[0] +
      "/" +
      data.deadline.split("-")[1] +
      "/" +
      data.deadline.split("-")[2];
    // Final deadline
    var deadlineF = deadline.split("T")[0];
    // Temporary orientation date
    var temp = data.orientation.split("T")[0];
    // Final orientation date
    var orientationD =
      temp.split("-")[0] + "/" + temp.split("-")[1] + "/" + temp.split("-")[2];
    // Final orientation Time
    var orientationT = moment(data.orientation).format("hh:mm");
    console.log("data", data);

    axios({
      method: "PUT",
      url: `${this.$axios.defaults.baseURL}/users/status/deployed/${id}`,
      headers: { Authorization: `Bearer ${token}` },
      data: {
        id: id,
        date: moment(data.deployment).format("LL")
        // dateDeadline: moment(data.deadline).format("LL"),
        // dateOrientation: moment(data.orientationD).format("LL"),
        // time: moment(data.orientationD).format("LT"),
        // contactNumber: data.contactN,
        // location: data.location,
        // message: notif
      }
    })
      .then(result => {
        axios({
          method: "POST",
          url: `${this.$axios.defaults.baseURL}/email/applicant/${id}/deployed`,
          headers: { Authorization: `Bearer ${token}` },
          data: {
            // id: id,
            date: moment(data.deployment).format("LL"),
            dateDeadline: moment(data.deadline).format("LL"),
            dateOrientation: moment(data.orientationD).format("LL"),
            time: moment(data.orientationD).format("LT"),
            contactNumber: data.contactN,
            location: data.location,
            message: notif
          }
        })
          .then(result => {
            const socket = io.connect(localStorage.serverLink, {
              secure: true
            });
            socket.emit("notificationAlerts", notifData);
            commit("deployDone", "Done");
          })
          .catch(error => {
            console.log(error);
            commit("deployDone", error);
          });
      })
      .catch(error => {
        console.log(error);
        commit("deployDone", error);
      });
  },

  // CHANGE DEPLOYMENT DATE
  changeDeployedDate({ commit }, { token, applicant, date, notifData }) {
    var applicantName = applicant.name.toLocaleUpperCase();
    var user = localStorage.fname + " " + localStorage.lname;
    user = user.toLocaleUpperCase();
    var id = applicant.id;
    var dateF =
      date.split("-")[0] + "/" + date.split("-")[1] + "/" + date.split("-")[2];
    axios({
      method: "PUT",
      url: `${this.$axios.defaults.baseURL}/users/deployed-date-update/${id}`,
      headers: { Authorization: `Bearer ${token}` },
      data: { date: dateF }
    })
      .then(result => {
        const socket = io.connect(localStorage.serverLink, { secure: true });
        socket.emit("notificationAlerts", notifData);
        commit("deployDateDone", "Done");
      })
      .catch(error => {
        console.log(error);
        commit("deployDateDone", error);
      });
  },

  // RATE AIF
  rateIAF({ commit }, { token, applicant, rating, notifData }) {
    var applicantName = applicant.name.toLocaleUpperCase();
    var user = localStorage.fname + " " + localStorage.lname;
    user = user.toLocaleUpperCase();
    var notif = user + " rated " + applicantName + "'s Assessment Form.";
    // console.log(notif);
    // console.log(rating[0]);

    axios({
      method: "POST",
      url: `${this.$axios.defaults.baseURL}/interview-assessment/add`,
      headers: { Authorization: `Bearer ${token}` },
      data: {
        user_id_applicant: rating[0].user_id_applicant,
        date_interview: rating[0].date_interview,

        communication_skills: rating[0].communication_skills,
        communication_skills_note: rating[0].communication_skills_note,
        confidence: rating[0].confidence,
        confidence_note: rating[0].confidence_note,
        physical_appearance: rating[0].physical_appearance,
        physical_appearance_note: rating[0].physical_appearance_note,
        knowledge_skills: rating[0].knowledge_skills,
        knowledge_skills_note: rating[0].knowledge_skills_note,

        asking_rate: rating[0].asking_rate,
        availability: rating[0].availability,
        others: rating[0].others,
        gen_remarks_recommendations: rating[0].gen_remarks_recommendations,

        user_id_interviewer: rating[0].user_id_interviewer
      }
    }).then(
      result => {
        // UPDATE APPLICANT STAGE
        axios({
          method: "PUT",
          url: `${this.$axios.defaults.baseURL}/users/status/pending/${rating[0].user_id_applicant}`,
          headers: { Authorization: `Bearer ${token}` }
        }).then(
          result => {
            // NOTIFY USERS
            console.log(notif);
            const socket = io.connect(localStorage.serverLink, {
              secure: true
            });
            socket.emit("notificationAlerts", notifData);
            commit("rate", "Done");
          },
          error => {
            console.log("==================================");
            console.log("Error while updating stage to 'Pending For Review'");
            console.log("==================================");
            console.log(error);
            console.log("==================================");
            commit("rate", error);
          }
        );
      },
      error => {
        console.log("==================================");
        console.log("Error while rating IAF");
        console.log("==================================");
        console.log(error.response.data.error);
        console.log("==================================");
        commit("rate", error.response.data.error);
      }
    );
  },

  // ADD REFERRAL
  addReferral({ commit }, { token, applicant, data }) {
    var applicantName = applicant.name.toLocaleUpperCase();
    var user = localStorage.fname + " " + localStorage.lname;
    user = user.toLocaleUpperCase();
    var notif = user + " tagged a referral form to " + applicantName + ".";
    // console.log(notif);
    // console.log(data);

    axios({
      method: "POST",
      url: `${this.$axios.defaults.baseURL}/users/referral-form/add`,
      headers: { Authorization: `Bearer ${token}` },
      data: {
        referrer_id: data.referrerID,
        applicant_id: data.referralID,
        referral_relationship_referrer: data.relationship
      }
    }).then(
      result => {
        // console.log("==================================");
        // console.log(result);
        // console.log("==================================");
        console.log(data.notifData);
        const socket = io.connect(localStorage.serverLink, { secure: true });
        socket.emit("notificationAlerts", data.notifData);
        commit("referralDone", "Done");
      },
      error => {
        console.log("==================================");
        console.log(error);
        console.log("==================================");
        commit("referralDone", error);
      }
    );
  },

  // RETURN APPLICANT
  returnApplicant({ commit }, { token, applicant, notifData }) {
    var applicantName = applicant.name.toLocaleUpperCase();
    var user = localStorage.fname + " " + localStorage.lname;
    user = user.toLocaleUpperCase();
    var notif = user + " returned " + applicantName + " to Received stage.";
    var id = applicant.id;
    // console.log(notif);
    // console.log(id);

    axios({
      method: "PATCH",
      url: `${this.$axios.defaults.baseURL}/users/status/received/${id}`,
      headers: { Authorization: `Bearer ${token}` }
    }).then(
      result => {
        // console.log("==================================");
        // console.log(result);
        // console.log("==================================");
        console.log(notif);
        const socket = io.connect(localStorage.serverLink, { secure: true });
        socket.emit("notificationAlerts", notifData);
        commit("returnApplicantDone", "Done");
      },
      error => {
        console.log("==================================");
        console.log(error);
        console.log("==================================");
        commit("returnApplicantDone", error);
      }
    );
  },
  // RETURN APPLICANT
  async getReceived(
    { commit },
    { token, dateFrom, dateTo, deptID, teamID, role, id }
  ) {
    return await axios({
      method: "POST",
      url: `${this.$axios.defaults.baseURL}/users/received`,
      headers: { Authorization: `Bearer ${token}` },
      data: {
        dateFrom,
        dateTo,
        deptID,
        teamID,
        role,
        id
      }
    }).then(
      result => {
        // console.log("==================================");
        // console.log(result);
        // console.log("==================================");
        // console.log(notif);
        // const socket = io.connect(localStorage.serverLink, { secure: true });
        // socket.emit("notificationAlerts", notifData);
        commit("getApplicantsAll", result.data.view);
        return result.data.view;
      },
      error => {
        console.log("==================================");
        console.log(error);
        console.log("==================================");
        commit("getApplicantsAll", error);
      }
    );
  }
};
