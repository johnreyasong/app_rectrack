export default {
  ADD_CATEGORY(state, data) {
    state.categoryState = data;
  },

  VIEW_CATEGORY(state, data) {
    state.categoryState = data;
  },
  VIEW_ALLCATEGORY(state, data) {
    state.categoryState = data;
  },
  EDIT_CATEGORY(state, data) {
    state.categoryState = data;
  },
  VIEW_QUESTIONTYPES(state, data) {
    state.questionTypesState = data;
  },
  ADD_QUESTIONS(state, data) {
    state.questionsState = data;
  }
};
