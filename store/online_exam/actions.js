import axios from "axios";

export default {
  //Fetch All Department
  async getCategoryDetails({ commit }, { token, id }) {
    return await axios({
      method: "GET",
      url: `${this.$axios.defaults.baseURL}/categories/view/${id}`,
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => {
        commit("VIEW_CATEGORY", res.data.view[0]);
        return res.data.view[0];
      })
      .catch(err => console.log(err));
  },
  async editCategory(
    { commit },
    { token, name, description, time_limit_on_seconds, teams_id, status, id }
  ) {
    return await axios({
      method: "PUT",
      url: `${this.$axios.defaults.baseURL}/categories/edit/${id}`,
      headers: {
        Authorization: `Bearer ${token}`
      },
      data: {
        name,
        description,
        time_limit_on_seconds,
        teams_id,
        status
      }
    }).then(res => {
      commit("EDIT_CATEGORY", res);
      return res;
    });
  },
  async addCategory(
    { commit },
    { token, name, description, time_limit_on_seconds, teams_id }
  ) {
    return await axios({
      method: "POST",
      url: `${this.$axios.defaults.baseURL}/categories/add`,
      headers: {
        Authorization: `Bearer ${token}`
      },
      data: {
        name,
        description,
        time_limit_on_seconds,
        teams_id
      }
    }).then(res => {
      commit("ADD_CATEGORY", res);
      return res;
    });
  },
  async getQuestionTypes({ commit }, { token }) {
    return await axios({
      method: "GET",
      url: `${this.$axios.defaults.baseURL}/question_types/view_all`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => {
        commit("VIEW_QUESTIONTYPES", res.data);
        // console.log("types:", res.data);
        return res.data;
      })
      .catch(err => console.log(err));
  },
  async getAllCategories({ commit }, { token }) {
    return await axios({
      method: "GET",
      url: `${this.$axios.defaults.baseURL}/categories/view`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => {
        commit("VIEW_CATEGORY", res.data.view);
        // console.log("cats:", res.data.view);
        return res.data.view;
      })
      .catch(err => console.log(err));
  },
  async addQuestionnaire(
    { commit },
    { token, questions, categories_id, details, question_type_id }
  ) {
    return await axios({
      method: "POST",
      url: `${this.$axios.defaults.baseURL}/questions/essay/add`,
      headers: {
        Authorization: `Bearer ${token}`
      },
      data: {
        categories_id,
        questions,
        details,
        question_type_id
      }
    }).then(res => {
      commit("ADD_QUESTIONS", res.data.posted);
      // console.log("addQ", res.data.posted);
      return res.data.posted;
    });
  },
  async addQuestionnaireMC(
    { commit },
    {
      token,
      questions,
      categories_id,
      details,
      question_type_id,
      correct_answer
    }
  ) {
    return await axios({
      method: "POST",
      url: `${this.$axios.defaults.baseURL}/questions/add`,
      headers: {
        Authorization: `Bearer ${token}`
      },
      data: {
        categories_id,
        questions,
        details,
        question_type_id,
        correct_answer
      }
    }).then(res => {
      commit("ADD_QUESTIONS", res.data.posted);
      // console.log("addQ", res.data.posted);
      return res.data.posted;
    });
  }
};
