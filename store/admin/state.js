export default () => ({
  departmentsState: [],
  allDepartmentsState: [],
  allTeamsState: [],
  teamsState: [],
  positionsState: [],
  systemUsersState: [],
  systemUsersDetailsState: []
});
