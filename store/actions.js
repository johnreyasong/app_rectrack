import axios from "axios";
import moment from "moment";

// LOGIN
export default {
  async loginUser({ commit }, { email, password }) {
    return await axios({
      method: "PUT",
      url: `${this.$axios.defaults.baseURL}/users/login`,
      headers: {
        "Access-Control-Allow-Origin": "*"
      },
      data: {
        email,
        password
      }
    }).then(res => {
      commit("LOGIN_USER", res.data.patched);
      // console.log(res.status);
      return res;
    });
  },

  //user firstname
  async getUserNameAction({ commit }, { token }) {
    if (localStorage.token == undefined) {
      console.log("Please login first");
    } else {
      return await axios({
        method: "GET",
        url: `${this.$axios.defaults.baseURL}/users/firstname/${token}`,
        headers: {
          Authorization: `Bearer ${token}`
        }
      }).then(res => {
        // console.log("Actions: user: " + res.data);
        localStorage.fname = res.data.view[0].firstname;
        localStorage.lname = res.data.view[0].lastname;
        commit("USER_NAME", res.data.view[0]);
        return res;
      });
    }
  },

  // VIEW ALL DEPARTMENT AFTER LOGIN
  viewAllDepartments({ commit }, { token }) {
    axios({
      method: "GET",
      url: `${this.$axios.defaults.baseURL}/departments/admin/view`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    }).then(res => {
      commit("viewDepartments", res.data);
    });
  },

  // VIEW POSITIONS BY DEPARTMENT
  viewPositionsByDept({ commit }, { data }) {
    // console.log(data[0].id);
    // console.log(data[0].token);
    axios({
      method: "GET",
      url: `${this.$axios.defaults.baseURL}/positions/view-by-department/${data[0].id}`,
      headers: {
        Authorization: `Bearer ${data[0].token}`
      }
    }).then(res => {
      commit("viewPositions", res.data);
    });
  },

  //GET ACCESS RIGHTS
  async AccessRightsAction({ commit }, { token, id }) {
    await axios({
      method: "GET",
      url: `${this.$axios.defaults.baseURL}/access-rights/admin/view/${id}`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    }).then(res => {
      commit("Access_Rights", res.data.posted[0]);

      console.log("getAccessRights", res.data.posted[0]);
    });
  },

  //POST ACTIVITY LOGS

  async activityLogsAction(
    { commit },
    { token, module, table_affected, activity }
  ) {
    await axios({
      method: "POST",
      url: `${this.$axios.defaults.baseURL}/users/add/logs`,
      headers: {
        Authorization: `Bearer ${token}`
      },
      data: {
        token,
        module,
        table_affected,
        activity
      }
    }).then(res => {
      commit("Activity_Logs", res.data);
      // console.log(res.data.posted.msg, "logs");
    });
  },

  // OPEN REFERRAL FORM
  viewReferralForm({ commit }, { data }) {
    commit("viewReferralFormInfo", data);
  },

  // ENTER EMPLOYEE CODE
  enterEmployeeCode({ commit }, { data }) {
    commit("enterEmployeeID", data);
  },

  // GET APPLICANT DETAILS
  viewApplicantDetails({ commit }, { applicantID, token }) {
    axios({
      method: "POST",
      url: `${this.$axios.defaults.baseURL}/users/find-applicant`,
      headers: {
        Authorization: `Bearer ${token}`
      },
      data: {
        deptID: localStorage.deptId ? localStorage.deptId : 1,
        teamID: localStorage.teamId ? localStorage.teamId : 1,
        id: localStorage.id ? localStorage.id : 1,
        role: localStorage.role,
        applicantID: applicantID
      }
    })
      .then(res => {
        // console.log("GET APPLICANT DETAILS: Success");
        // console.log(res.data.response);
        commit("viewApplicantInfo", res.data.response);
      })
      .catch(err => {
        console.log("GET APPLICANT DETAILS");
        console.log(err);
      });
  },

  // GET USER USAGE DATA
  async viewUserUsage({ commit }, {userID, token, dateTo, user, data}) {
    // SETTING USER DATA USAGE STATUS (Fetching, Filled or Empty)
    commit("setUserDataStatus", "Fetching:2");
    
    var list = [];
    var userData = [];
    var month = moment(dateTo).format("MMMM");
    var mon = moment(dateTo).format("M");
    var year = moment(dateTo).format("YYYY");
    var monYear = moment(dateTo).format("MMMM YYYY");
    var from = moment(dateTo).startOf('month').format('YYYY-MM-DD');
    var to = moment(dateTo).endOf('month').format('YYYY-MM-DD');

    // console.log("==================================");
    // console.log("View User Usage Data");
    // console.log("==================================");
    // console.log(userID);
    // console.log(from);
    // console.log(to);
    // console.log(user);
    // console.log(data);
    // console.log("==================================");
    // GET TOTAL LOGINS
    await axios({
      method: "POST",
      url: `${this.$axios.defaults.baseURL}/users/view/logs`,
      headers: { 
        "Content-Type": "application/json", 
        Authorization: "Bearer " + token
      },
      data: { date: from, dateTo: to }
    }).then(
      result => {
        // console.log(result.data.posted);
        var list = result.data.posted;
        
        if(list.length > 0) {
          var filteredData = list.filter(data => {
            return ( data.name == user && data.module == "Login");
          });
          
        }
        else {
          var filteredData = [];
        }
        // SET USER'S FILTERED ACTIVITY DATA TO STATE
        commit("filteredLogs", filteredData);
      },
      error => { console.log(error);  }
    );
    // GET LIST
    await axios({
      method: "GET",
      url: `${this.$axios.defaults.baseURL}/users/notifications/get?from=${from}&to=${to}`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    }).then(
      res => {
        list = res.data.get;
        var cnt = 0;

        // FILTER USER'S ACTIVITY DATA ONLY
        if(list.length > 0) {
          var filteredData = list.filter(data => {
            return data.userID == userID;
          });
        }
        else {
          var filteredData = [];
        }
        // STORE USER DATA/INFO
        userData = {
          id: userID,
          dateFrom: from,
          dateTo: to,
          month: month,
          year: year,
          monYear: monYear,
          mon: mon,
          
          user: user,
          team: data.userTeam,
          dep: data.userDepartment,
          role: data.userRole
        }
        // SET USER DATA/INFO TO STATE
        commit("setUserData", userData);

        // SET USER'S FILTERED ACTIVITY DATA TO STATE
        commit("viewUserUsageData", filteredData);

        // SET USER DATA USAGE STATUS
        if(filteredData.length > 0) {
          commit("setUserDataStatus", "Filled:1");
        }
        else {
          commit("setUserDataStatus", "Empty:-1");
        }
      },
      error => {
        commit("setUserDataStatus", "Error:" + error);
        console.log("GET NOTIFS");
        console.log(error);
      });
  },

  // GET ONLINE EXAM DATA FOR RATING
  viewExamData({ commit }, { applicantID, token }) {
    // GET POSITION ID FIRST
    axios({
      method: "POST",
      url: `${this.$axios.defaults.baseURL}/users/find-applicant`,
      headers: {
        Authorization: `Bearer ${token}`
      },
      data: {
        deptID: localStorage.deptId ? localStorage.deptId : 1,
        teamID: localStorage.teamId ? localStorage.teamId : 1,
        id: localStorage.id ? localStorage.id : 1,
        role: localStorage.role,
        applicantID: applicantID
      }
    })
      .then(res => {
        // THEN GET EXAM DATA
        console.log("GET APPLICANT DETAILS: Success");
        console.log(res.data.response);
        let posID = res.data.response.applicantInformation[0].positionID;
        axios({
          method: "GET",
          url: `${this.$axios.defaults.baseURL}/users/online-exam-result/${applicantID}/${posID}/online-exam`,
          headers: { Authorization: `Bearer ${token}` }
        }).then(
          res => {
            // THEN GET EXAM DATA
            console.log("==============================");
            console.log("GET EXAM DATA: Success");
            console.log("==============================");
            console.log(res);
            console.log("==============================");

            commit("examRating", res.data.view);
          },
          error => {
            console.log("GET EXAM DATA: Error");
            console.log(error);
          }
        );
      })
      .catch(err => {
        console.log("GET APPLICANT DETAILS: Error");
        console.log(err);
      });
  },

  // GET QUESTION DETAILS
  async viewQuestionDetails({ commit }, { questionID, token, type }) {
    var quest;
    var catg;
    var team;
    var questInfo = [];

    console.log("=======================================");
    // FETCH QUESTION BY ID
    await axios({
      method: "GET",
      url: `${this.$axios.defaults.baseURL}/questions/view/${questionID}`,
      headers: { Authorization: `Bearer ${token}` },
      data: { type }
    }).then(
      res => {
        console.log("res", res);
        console.log("FETCH QUESTION: SUCCESS");
        console.log("=======================================");
        quest = res.data[Object.keys(res.data)[0]];
        // commit("viewQuestionInfo", res.data.view[0]);
      },
      err => {
        console.log("FETCH QUESTION: FAILED");
        console.log(err);
        quest = true;
      }
    );
    console.log(quest[0].catgId);
    console.log(quest);
    console.log("=======================================");

    // FETCH CATEGORY BY ID
    await axios({
      method: "GET",
      url: `${this.$axios.defaults.baseURL}/categories/view/${quest[0].catgId}`,
      headers: { Authorization: `Bearer ${token}` }
    }).then(
      res => {
        console.log("FETCH CATEGORY: SUCCESS");
        console.log("=======================================");
        catg = res.data[Object.keys(res.data)[0]];
        // commit("viewApplicantInfo", res.data.response);
      },
      error => {
        console.log("FETCH CATEGORY: FAILED");
        console.log(error);
        catg = true;
      }
    );
    console.log(catg[0].teams_id);
    console.log(catg);
    console.log("=======================================");

    // FETCH TEAM BY ID (WITH DEPARTMENT ID AND NAME)
    await axios({
      method: "GET",
      url: `${this.$axios.defaults.baseURL}/teams/admin/view/${catg[0].teams_id}`,
      headers: { Authorization: `Bearer ${token}` }
    }).then(
      res => {
        console.log("FETCH TEAM: SUCCESS");
        console.log("=======================================");
        team = res.data[Object.keys(res.data)[0]];
        // commit("viewApplicantInfo", res.data.response);
      },
      error => {
        console.log("FETCH TEAM: FAILED");
        console.log(error);
        team = true;
      }
    );
    console.log(team[0].department_id);
    console.log(team);
    console.log("=======================================");

    // PUSH INTO ONE ARRAY
    if (quest == true || catg == true || team == true) {
      console.log("quest: " + quest + "| catg: " + "| team: " + team);
      questInfo = { noData: true, mess: "Failed to fetch data" };
    } else if (quest[0].details != undefined) {
      questInfo.push({
        ...quest[0],
        catgName: catg[0].name,
        teamId: catg[0].teams_id,
        teamName: team[0].name,
        deptId: team[0].department_id,
        deptName: team[0].deptname,
        type: type
      });
    } else {
      questInfo.push({
        ...quest[0],
        catgName: catg[0].name,
        teamId: catg[0].teams_id,
        teamName: team[0].name,
        deptId: team[0].department_id,
        deptName: team[0].deptname,
        type: type,
        details: []
      });
    }

    // PASS DETAILS INTO STATE THROUGH MUTATION
    commit("viewQuestionInfo", questInfo);
    console.log(questInfo);
  },
  // REGISTER WALK-IN APPLICANT
  addApplicantDetails({ commit }, { user, token }) {
    console.log(user);
  },

  async filterDept({ commit }, { deptId, token, stage }) {
    // filter by department
    console.log("stage: " + stage);
    console.log("deptID: " + deptId);
    return await axios({
      method: "GET",
      url: `${this.$axios.defaults.baseURL}/users/${stage}/filter/${deptId}`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    .then(res => {
      // THEN GET EXAM DATA
      
      commit("FILTER_DEPT", res.data[Object.keys(res.data)[0]]);
      return res.data[Object.keys(res.data)[0]];
    })
    .catch(err => {
      console.log(err);
    });
  },
  
  async filterDeptPos({ commit }, { deptId, posId, token, stage }) {
    // filter position by department
    console.log("stage: " + stage);
    console.log("deptID: " + deptId);
    return await axios({
      method: "GET",
      url: `${this.$axios.defaults.baseURL}/users/${stage}/filter/${deptId}/${posId}`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => {
        // THEN GET EXAM DATA

        commit("FILTER_DEPT", res.data[Object.keys(res.data)[0]]);
        return res.data[Object.keys(res.data)[0]];
      })
      .catch(err => {
        console.log(err);
      });
  },
  async keptForRefReports(
    { commit },
    { id, role, deptID, teamID, dateFrom, dateTo, token }
  ) {
    // filter position by department
    return await axios({
      method: "POST",
      url: `${this.$axios.defaults.baseURL}/users/kept-reference`,
      headers: {
        Authorization: `Bearer ${token}`
      },
      data: {
        id,
        role,
        deptID,
        teamID,
        dateFrom,
        dateTo
      }
    })
      .then(res => {
        commit("KEPTFORREF_REPORTS", res.data[Object.keys(res.data)[0]]);
        return res.data[Object.keys(res.data)[0]];
      })
      .catch(err => {
        console.log(err);
      });
  }
};
