export default {
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: false,

  target: "static",

  // loading: '~/components/loader.vue',
  loading: {
    color: "#a6ce39",
    height: "5px"
  },

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: "RecTrack V3",
    htmlAttrs: {
      lang: "en"
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" }
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    "@/assets/mainStyle.scss",
    "@/assets/sideBar.scss",
    "@/assets/header.scss",
    "@/assets/module.scss",
    "@/assets/inputs.scss",
    "@/assets/exams.scss",
    "@/assets/admin.scss",
    "@/assets/viewApplicants.scss",
    "@/assets/vue-multiple-select.scss",
    "@/assets/userUsage.scss",
    "@/assets/applicationForm.scss"
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: ["@/plugins/Vuelidate"],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: ["@nuxtjs/fontawesome"],
  fontawesome: {
    component: "fa",
    icons: {
      solid: [
        "faSignOutAlt",
        "faSignInAlt",
        "faCashRegister",
        "faBoxes",
        "faPowerOff",
        "faFile",
        "faChartLine",
        "faChartBar",
        "faChartPie",
        "faAngleLeft",
        "faAngleRight",
        "faTruck",
        "faBoxOpen",
        "faPlusCircle",
        "faEraser",
        "faChevronCircleRight",
        "faReceipt",
        "faExchangeAlt",
        "faHistory",
        "faStore",
        "faCartPlus",
        "faCartArrowDown",
        "faShoppingCart",
        "faTrash",
        "faClipboard",
        "faClipboardCheck",
        "faTag",
        "faEye",
        "faEyeSlash",
        "faMoneyBillWaveAlt",
        "faSave",
        "faDoorOpen",
        "faArrowAltCircleRight",
        "faDesktop",
        "faBars",
        "faUserShield",

        "faIdCardAlt",
        "faIdCard",
        "faUsers",
        "faUser",
        "faUserCog",
        "faKey",
        "faClipboardList",
        "faMouse",
        "faListOl",
        "faFileAlt",
        "faSitemap",
        "faBrain",
        "faFileExport",
        "faCopy",
        "faAddressCard",
        "faPeopleArrows",
        "faWalking",
        "faFileSignature",
        "faBell",
        "faCaretUp",
        "faCaretDown",
        "faCaretRight",
        "faCaretLeft",
        "faAngleDoubleLeft",
        "faAngleDoubleRight",
        "faSearch",
        "faEllipsisV",
        "faBan",
        "faUserTimes",
        "faEdit",
        "faRedoAlt",
        "faExclamationTriangle",
        "faClock",
        "faChevronRight",
        "faChevronLeft",
        "faCheck",
        "faCheckCircle",
        "faCheckSquare",
        "faSquare",
        "faTimesCircle",
        "faPlus",
        "faMinus",
        "faTimes",
        "faCircle",
        "faArchive",
        "faUserCircle",
        "faBuilding",
        "faFileExcel",
        "faInfoCircle",
        "faPrint",
        "faBookmark",
        "faThumbsDown",
        "faFolder",
        "faCalendarDay",
        "faUserCheck",
        "faBriefcase",
        "faRocket",
        "faCalendarCheck",
        "faPercentage",
        "faUserTag",
        "faEnvelope",
        "faUserTie"
      ],
      brands: [
        "faGoogle",
        "faLinkedin",
        "faTwitter",
        "faFacebook",
        "faInstagram",
        "faPinterest"
      ]
    }
  },

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/bootstrap // removed
    "bootstrap-vue/nuxt",
    "@nuxtjs/axios"
  ],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    extractCSS: process.env.NODE_ENV !== "development"
  },
  server: {
    port: 3300
  },
  axios: {
    // baseURL: "http://172.16.4.114:5095/rectrack-sqav2" // Sir Rumel
    // baseURL: "https://rectrack-eut.biotechfarms.net/rectrack-sqav2" //v2,
    // baseURL: "http://172.16.28.2:5095/rectrack-sqav2" //v2 coach
    // baseURL: "http://172.16.4.182:7777/rectrack-sqa" //butch_dev
    // baseURL: "https://94.237.65.245:5095/rectrack-sqav2" // try
    baseURL: "https://rectrack-v2.biotechfarms.net/rectrack-prod" // PRODUCTION
  },
  env: {
    // baseURL: "http://172.16.28.2:5095" // try
    baseURL: "https://rectrack-v2.biotechfarms.net"
    // baseURL: "https://rectrack-eut.biotechfarms.net"
  },
  router: {
    extendRoutes(routes, resolve) {
      routes.push({
        name: "custom",
        path: "*",
        component: resolve(__dirname, "pages/404.vue")
      });
    }
  }
};
